const express = require('express')
const router = express.Router()

/* GET users listing. */
router.get('/:message', function (req, res, next) {
  const { params } = req
  res.json({
    message: 'Hello Ekarat ', params
  })
})

module.exports = router
